***Dice Magic*** is an open source, cryptographically strong, dice rolling chatbot. Designed with tabletop gaming in mind.

{{% install %}}