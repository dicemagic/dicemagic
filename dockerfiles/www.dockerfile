FROM nginx:1.19-alpine

COPY out/include/www/content /usr/share/nginx/html
COPY out/include/www/nginx.conf /etc/nginx/nginx.conf
COPY out/include/www/mime.types /etc/nginx/conf/mime.types

EXPOSE 8080
EXPOSE 8081
EXPOSE 8082