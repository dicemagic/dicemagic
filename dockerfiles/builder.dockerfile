FROM golang:1.16-alpine
RUN apk add --no-cache \
            build-base make git zip unzip
COPY . /dicemagic
WORKDIR /dicemagic
RUN go get -u golang.org/x/lint/golint && \
    go get github.com/gohugoio/hugo && \
    go install github.com/gohugoio/hugo@latest && \
    go get gonum.org/v1/gonum && \
    go get golang.org/x/exp && \
    go get golang.org/x/net && \
    go get github.com/golang/protobuf/protoc-gen-go && \
    go get golang.org/x/tools/cmd/stringer && \
    go get ./...

FROM golang:1.16-buster
RUN apt-get update && apt-get install -y --no-install-recommends \
    unzip zip \
    && rm -rf /var/lib/apt/lists/*
COPY --from=0 /go/ /go/