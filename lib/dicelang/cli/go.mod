module gitlab.com/dicemagic/dicemagic/lib/dicelang/cli

go 1.16

replace gitlab.com/dicemagic/dicemagic/lib/dicelang-errors v0.0.0 => ../../dicelang-errors

replace gitlab.com/dicemagic/dicemagic/lib/dicelang v0.0.0 => ../

require (
	gitlab.com/dicemagic/dicemagic/lib/dicelang v0.0.0
	gitlab.com/dicemagic/dicemagic/lib/dicelang-errors v0.0.0
)
