// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.12.0
// source: dicelang.proto

package dicelang

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// The request message containing the command. Input validation preformed on the server side.
type RollRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Cmd           string `protobuf:"bytes,1,opt,name=cmd,proto3" json:"cmd,omitempty"`
	Probabilities bool   `protobuf:"varint,2,opt,name=probabilities,proto3" json:"probabilities,omitempty"`
	Chart         bool   `protobuf:"varint,3,opt,name=chart,proto3" json:"chart,omitempty"`
}

func (x *RollRequest) Reset() {
	*x = RollRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dicelang_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RollRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RollRequest) ProtoMessage() {}

func (x *RollRequest) ProtoReflect() protoreflect.Message {
	mi := &file_dicelang_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RollRequest.ProtoReflect.Descriptor instead.
func (*RollRequest) Descriptor() ([]byte, []int) {
	return file_dicelang_proto_rawDescGZIP(), []int{0}
}

func (x *RollRequest) GetCmd() string {
	if x != nil {
		return x.Cmd
	}
	return ""
}

func (x *RollRequest) GetProbabilities() bool {
	if x != nil {
		return x.Probabilities
	}
	return false
}

func (x *RollRequest) GetChart() bool {
	if x != nil {
		return x.Chart
	}
	return false
}

// The response message containing one DiceSet. If the command warrents multiple dice-sets, they will be merged
type RollResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Cmd      string     `protobuf:"bytes,1,opt,name=Cmd,proto3" json:"Cmd,omitempty"`
	DiceSets []*DiceSet `protobuf:"bytes,2,rep,name=DiceSets,proto3" json:"DiceSets,omitempty"`
	Ok       bool       `protobuf:"varint,3,opt,name=Ok,proto3" json:"Ok,omitempty"`
	Error    *RollError `protobuf:"bytes,4,opt,name=Error,proto3" json:"Error,omitempty"`
}

func (x *RollResponse) Reset() {
	*x = RollResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dicelang_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RollResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RollResponse) ProtoMessage() {}

func (x *RollResponse) ProtoReflect() protoreflect.Message {
	mi := &file_dicelang_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RollResponse.ProtoReflect.Descriptor instead.
func (*RollResponse) Descriptor() ([]byte, []int) {
	return file_dicelang_proto_rawDescGZIP(), []int{1}
}

func (x *RollResponse) GetCmd() string {
	if x != nil {
		return x.Cmd
	}
	return ""
}

func (x *RollResponse) GetDiceSets() []*DiceSet {
	if x != nil {
		return x.DiceSets
	}
	return nil
}

func (x *RollResponse) GetOk() bool {
	if x != nil {
		return x.Ok
	}
	return false
}

func (x *RollResponse) GetError() *RollError {
	if x != nil {
		return x.Error
	}
	return nil
}

type Dice struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count         int64             `protobuf:"varint,1,opt,name=Count,proto3" json:"Count,omitempty"`
	Sides         int64             `protobuf:"varint,2,opt,name=Sides,proto3" json:"Sides,omitempty"`
	Total         int64             `protobuf:"varint,3,opt,name=Total,proto3" json:"Total,omitempty"`
	Faces         []int64           `protobuf:"varint,4,rep,packed,name=Faces,proto3" json:"Faces,omitempty"`
	Color         string            `protobuf:"bytes,5,opt,name=Color,proto3" json:"Color,omitempty"`
	Max           int64             `protobuf:"varint,6,opt,name=Max,proto3" json:"Max,omitempty"`
	Min           int64             `protobuf:"varint,7,opt,name=Min,proto3" json:"Min,omitempty"`
	DropHighest   int64             `protobuf:"varint,8,opt,name=DropHighest,proto3" json:"DropHighest,omitempty"`
	DropLowest    int64             `protobuf:"varint,9,opt,name=DropLowest,proto3" json:"DropLowest,omitempty"`
	Chart         []byte            `protobuf:"bytes,10,opt,name=Chart,proto3" json:"Chart,omitempty"`
	Probabilities map[int64]float64 `protobuf:"bytes,11,rep,name=Probabilities,proto3" json:"Probabilities,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"fixed64,2,opt,name=value,proto3"`
}

func (x *Dice) Reset() {
	*x = Dice{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dicelang_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Dice) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Dice) ProtoMessage() {}

func (x *Dice) ProtoReflect() protoreflect.Message {
	mi := &file_dicelang_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Dice.ProtoReflect.Descriptor instead.
func (*Dice) Descriptor() ([]byte, []int) {
	return file_dicelang_proto_rawDescGZIP(), []int{2}
}

func (x *Dice) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *Dice) GetSides() int64 {
	if x != nil {
		return x.Sides
	}
	return 0
}

func (x *Dice) GetTotal() int64 {
	if x != nil {
		return x.Total
	}
	return 0
}

func (x *Dice) GetFaces() []int64 {
	if x != nil {
		return x.Faces
	}
	return nil
}

func (x *Dice) GetColor() string {
	if x != nil {
		return x.Color
	}
	return ""
}

func (x *Dice) GetMax() int64 {
	if x != nil {
		return x.Max
	}
	return 0
}

func (x *Dice) GetMin() int64 {
	if x != nil {
		return x.Min
	}
	return 0
}

func (x *Dice) GetDropHighest() int64 {
	if x != nil {
		return x.DropHighest
	}
	return 0
}

func (x *Dice) GetDropLowest() int64 {
	if x != nil {
		return x.DropLowest
	}
	return 0
}

func (x *Dice) GetChart() []byte {
	if x != nil {
		return x.Chart
	}
	return nil
}

func (x *Dice) GetProbabilities() map[int64]float64 {
	if x != nil {
		return x.Probabilities
	}
	return nil
}

type DiceSet struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Dice          []*Dice            `protobuf:"bytes,1,rep,name=Dice,proto3" json:"Dice,omitempty"`
	TotalsByColor map[string]float64 `protobuf:"bytes,2,rep,name=TotalsByColor,proto3" json:"TotalsByColor,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"fixed64,2,opt,name=value,proto3"`
	Total         int64              `protobuf:"varint,3,opt,name=Total,proto3" json:"Total,omitempty"`
	ReString      string             `protobuf:"bytes,4,opt,name=ReString,proto3" json:"ReString,omitempty"`
	DropHighest   int64              `protobuf:"varint,5,opt,name=DropHighest,proto3" json:"DropHighest,omitempty"`
	DropLowest    int64              `protobuf:"varint,6,opt,name=DropLowest,proto3" json:"DropLowest,omitempty"`
	Colors        []string           `protobuf:"bytes,7,rep,name=Colors,proto3" json:"Colors,omitempty"`
	ColorDepth    int32              `protobuf:"varint,8,opt,name=ColorDepth,proto3" json:"ColorDepth,omitempty"`
}

func (x *DiceSet) Reset() {
	*x = DiceSet{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dicelang_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DiceSet) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DiceSet) ProtoMessage() {}

func (x *DiceSet) ProtoReflect() protoreflect.Message {
	mi := &file_dicelang_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DiceSet.ProtoReflect.Descriptor instead.
func (*DiceSet) Descriptor() ([]byte, []int) {
	return file_dicelang_proto_rawDescGZIP(), []int{3}
}

func (x *DiceSet) GetDice() []*Dice {
	if x != nil {
		return x.Dice
	}
	return nil
}

func (x *DiceSet) GetTotalsByColor() map[string]float64 {
	if x != nil {
		return x.TotalsByColor
	}
	return nil
}

func (x *DiceSet) GetTotal() int64 {
	if x != nil {
		return x.Total
	}
	return 0
}

func (x *DiceSet) GetReString() string {
	if x != nil {
		return x.ReString
	}
	return ""
}

func (x *DiceSet) GetDropHighest() int64 {
	if x != nil {
		return x.DropHighest
	}
	return 0
}

func (x *DiceSet) GetDropLowest() int64 {
	if x != nil {
		return x.DropLowest
	}
	return 0
}

func (x *DiceSet) GetColors() []string {
	if x != nil {
		return x.Colors
	}
	return nil
}

func (x *DiceSet) GetColorDepth() int32 {
	if x != nil {
		return x.ColorDepth
	}
	return 0
}

type DiceSets struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	DiceSet []*DiceSet `protobuf:"bytes,1,rep,name=DiceSet,proto3" json:"DiceSet,omitempty"`
}

func (x *DiceSets) Reset() {
	*x = DiceSets{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dicelang_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DiceSets) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DiceSets) ProtoMessage() {}

func (x *DiceSets) ProtoReflect() protoreflect.Message {
	mi := &file_dicelang_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DiceSets.ProtoReflect.Descriptor instead.
func (*DiceSets) Descriptor() ([]byte, []int) {
	return file_dicelang_proto_rawDescGZIP(), []int{4}
}

func (x *DiceSets) GetDiceSet() []*DiceSet {
	if x != nil {
		return x.DiceSet
	}
	return nil
}

type RollError struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Msg  string `protobuf:"bytes,1,opt,name=msg,proto3" json:"msg,omitempty"`
	Code int32  `protobuf:"varint,2,opt,name=code,proto3" json:"code,omitempty"`
}

func (x *RollError) Reset() {
	*x = RollError{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dicelang_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RollError) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RollError) ProtoMessage() {}

func (x *RollError) ProtoReflect() protoreflect.Message {
	mi := &file_dicelang_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RollError.ProtoReflect.Descriptor instead.
func (*RollError) Descriptor() ([]byte, []int) {
	return file_dicelang_proto_rawDescGZIP(), []int{5}
}

func (x *RollError) GetMsg() string {
	if x != nil {
		return x.Msg
	}
	return ""
}

func (x *RollError) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

var File_dicelang_proto protoreflect.FileDescriptor

var file_dicelang_proto_rawDesc = []byte{
	0x0a, 0x0e, 0x64, 0x69, 0x63, 0x65, 0x6c, 0x61, 0x6e, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x22, 0x5b, 0x0a, 0x0b, 0x52, 0x6f, 0x6c, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x10, 0x0a, 0x03, 0x63, 0x6d, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x63, 0x6d,
	0x64, 0x12, 0x24, 0x0a, 0x0d, 0x70, 0x72, 0x6f, 0x62, 0x61, 0x62, 0x69, 0x6c, 0x69, 0x74, 0x69,
	0x65, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x08, 0x52, 0x0d, 0x70, 0x72, 0x6f, 0x62, 0x61, 0x62,
	0x69, 0x6c, 0x69, 0x74, 0x69, 0x65, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x68, 0x61, 0x72, 0x74,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x08, 0x52, 0x05, 0x63, 0x68, 0x61, 0x72, 0x74, 0x22, 0x78, 0x0a,
	0x0c, 0x52, 0x6f, 0x6c, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x10, 0x0a,
	0x03, 0x43, 0x6d, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x43, 0x6d, 0x64, 0x12,
	0x24, 0x0a, 0x08, 0x44, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x08, 0x2e, 0x44, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x52, 0x08, 0x44, 0x69, 0x63,
	0x65, 0x53, 0x65, 0x74, 0x73, 0x12, 0x0e, 0x0a, 0x02, 0x4f, 0x6b, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x08, 0x52, 0x02, 0x4f, 0x6b, 0x12, 0x20, 0x0a, 0x05, 0x45, 0x72, 0x72, 0x6f, 0x72, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x0a, 0x2e, 0x52, 0x6f, 0x6c, 0x6c, 0x45, 0x72, 0x72, 0x6f, 0x72,
	0x52, 0x05, 0x45, 0x72, 0x72, 0x6f, 0x72, 0x22, 0xf2, 0x02, 0x0a, 0x04, 0x44, 0x69, 0x63, 0x65,
	0x12, 0x14, 0x0a, 0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x05, 0x43, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x53, 0x69, 0x64, 0x65, 0x73, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x53, 0x69, 0x64, 0x65, 0x73, 0x12, 0x14, 0x0a, 0x05,
	0x54, 0x6f, 0x74, 0x61, 0x6c, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x54, 0x6f, 0x74,
	0x61, 0x6c, 0x12, 0x14, 0x0a, 0x05, 0x46, 0x61, 0x63, 0x65, 0x73, 0x18, 0x04, 0x20, 0x03, 0x28,
	0x03, 0x52, 0x05, 0x46, 0x61, 0x63, 0x65, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x43, 0x6f, 0x6c, 0x6f,
	0x72, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x12, 0x10,
	0x0a, 0x03, 0x4d, 0x61, 0x78, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03, 0x4d, 0x61, 0x78,
	0x12, 0x10, 0x0a, 0x03, 0x4d, 0x69, 0x6e, 0x18, 0x07, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03, 0x4d,
	0x69, 0x6e, 0x12, 0x20, 0x0a, 0x0b, 0x44, 0x72, 0x6f, 0x70, 0x48, 0x69, 0x67, 0x68, 0x65, 0x73,
	0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0b, 0x44, 0x72, 0x6f, 0x70, 0x48, 0x69, 0x67,
	0x68, 0x65, 0x73, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x44, 0x72, 0x6f, 0x70, 0x4c, 0x6f, 0x77, 0x65,
	0x73, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a, 0x44, 0x72, 0x6f, 0x70, 0x4c, 0x6f,
	0x77, 0x65, 0x73, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x43, 0x68, 0x61, 0x72, 0x74, 0x18, 0x0a, 0x20,
	0x01, 0x28, 0x0c, 0x52, 0x05, 0x43, 0x68, 0x61, 0x72, 0x74, 0x12, 0x3e, 0x0a, 0x0d, 0x50, 0x72,
	0x6f, 0x62, 0x61, 0x62, 0x69, 0x6c, 0x69, 0x74, 0x69, 0x65, 0x73, 0x18, 0x0b, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x18, 0x2e, 0x44, 0x69, 0x63, 0x65, 0x2e, 0x50, 0x72, 0x6f, 0x62, 0x61, 0x62, 0x69,
	0x6c, 0x69, 0x74, 0x69, 0x65, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x0d, 0x50, 0x72, 0x6f,
	0x62, 0x61, 0x62, 0x69, 0x6c, 0x69, 0x74, 0x69, 0x65, 0x73, 0x1a, 0x40, 0x0a, 0x12, 0x50, 0x72,
	0x6f, 0x62, 0x61, 0x62, 0x69, 0x6c, 0x69, 0x74, 0x69, 0x65, 0x73, 0x45, 0x6e, 0x74, 0x72, 0x79,
	0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03, 0x6b,
	0x65, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x01, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x22, 0xd5, 0x02, 0x0a,
	0x07, 0x44, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x12, 0x19, 0x0a, 0x04, 0x44, 0x69, 0x63, 0x65,
	0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x05, 0x2e, 0x44, 0x69, 0x63, 0x65, 0x52, 0x04, 0x44,
	0x69, 0x63, 0x65, 0x12, 0x41, 0x0a, 0x0d, 0x54, 0x6f, 0x74, 0x61, 0x6c, 0x73, 0x42, 0x79, 0x43,
	0x6f, 0x6c, 0x6f, 0x72, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x44, 0x69, 0x63,
	0x65, 0x53, 0x65, 0x74, 0x2e, 0x54, 0x6f, 0x74, 0x61, 0x6c, 0x73, 0x42, 0x79, 0x43, 0x6f, 0x6c,
	0x6f, 0x72, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x0d, 0x54, 0x6f, 0x74, 0x61, 0x6c, 0x73, 0x42,
	0x79, 0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x12, 0x14, 0x0a, 0x05, 0x54, 0x6f, 0x74, 0x61, 0x6c, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x54, 0x6f, 0x74, 0x61, 0x6c, 0x12, 0x1a, 0x0a, 0x08,
	0x52, 0x65, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x52, 0x65, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x12, 0x20, 0x0a, 0x0b, 0x44, 0x72, 0x6f, 0x70,
	0x48, 0x69, 0x67, 0x68, 0x65, 0x73, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0b, 0x44,
	0x72, 0x6f, 0x70, 0x48, 0x69, 0x67, 0x68, 0x65, 0x73, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x44, 0x72,
	0x6f, 0x70, 0x4c, 0x6f, 0x77, 0x65, 0x73, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a,
	0x44, 0x72, 0x6f, 0x70, 0x4c, 0x6f, 0x77, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x43, 0x6f,
	0x6c, 0x6f, 0x72, 0x73, 0x18, 0x07, 0x20, 0x03, 0x28, 0x09, 0x52, 0x06, 0x43, 0x6f, 0x6c, 0x6f,
	0x72, 0x73, 0x12, 0x1e, 0x0a, 0x0a, 0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x44, 0x65, 0x70, 0x74, 0x68,
	0x18, 0x08, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0a, 0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x44, 0x65, 0x70,
	0x74, 0x68, 0x1a, 0x40, 0x0a, 0x12, 0x54, 0x6f, 0x74, 0x61, 0x6c, 0x73, 0x42, 0x79, 0x43, 0x6f,
	0x6c, 0x6f, 0x72, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61,
	0x6c, 0x75, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x01, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65,
	0x3a, 0x02, 0x38, 0x01, 0x22, 0x2e, 0x0a, 0x08, 0x44, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x73,
	0x12, 0x22, 0x0a, 0x07, 0x44, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x18, 0x01, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x08, 0x2e, 0x44, 0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x52, 0x07, 0x44, 0x69, 0x63,
	0x65, 0x53, 0x65, 0x74, 0x22, 0x31, 0x0a, 0x09, 0x52, 0x6f, 0x6c, 0x6c, 0x45, 0x72, 0x72, 0x6f,
	0x72, 0x12, 0x10, 0x0a, 0x03, 0x6d, 0x73, 0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03,
	0x6d, 0x73, 0x67, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x32, 0x2f, 0x0a, 0x06, 0x52, 0x6f, 0x6c, 0x6c, 0x65,
	0x72, 0x12, 0x25, 0x0a, 0x04, 0x52, 0x6f, 0x6c, 0x6c, 0x12, 0x0c, 0x2e, 0x52, 0x6f, 0x6c, 0x6c,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x0d, 0x2e, 0x52, 0x6f, 0x6c, 0x6c, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x2b, 0x5a, 0x29, 0x67, 0x69, 0x74, 0x68,
	0x75, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x61, 0x61, 0x73, 0x6d, 0x61, 0x6c, 0x6c, 0x2f, 0x64,
	0x69, 0x63, 0x65, 0x6d, 0x61, 0x67, 0x69, 0x63, 0x2f, 0x6c, 0x69, 0x62, 0x2f, 0x64, 0x69, 0x63,
	0x65, 0x6c, 0x61, 0x6e, 0x67, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_dicelang_proto_rawDescOnce sync.Once
	file_dicelang_proto_rawDescData = file_dicelang_proto_rawDesc
)

func file_dicelang_proto_rawDescGZIP() []byte {
	file_dicelang_proto_rawDescOnce.Do(func() {
		file_dicelang_proto_rawDescData = protoimpl.X.CompressGZIP(file_dicelang_proto_rawDescData)
	})
	return file_dicelang_proto_rawDescData
}

var file_dicelang_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_dicelang_proto_goTypes = []interface{}{
	(*RollRequest)(nil),  // 0: RollRequest
	(*RollResponse)(nil), // 1: RollResponse
	(*Dice)(nil),         // 2: Dice
	(*DiceSet)(nil),      // 3: DiceSet
	(*DiceSets)(nil),     // 4: DiceSets
	(*RollError)(nil),    // 5: RollError
	nil,                  // 6: Dice.ProbabilitiesEntry
	nil,                  // 7: DiceSet.TotalsByColorEntry
}
var file_dicelang_proto_depIdxs = []int32{
	3, // 0: RollResponse.DiceSets:type_name -> DiceSet
	5, // 1: RollResponse.Error:type_name -> RollError
	6, // 2: Dice.Probabilities:type_name -> Dice.ProbabilitiesEntry
	2, // 3: DiceSet.Dice:type_name -> Dice
	7, // 4: DiceSet.TotalsByColor:type_name -> DiceSet.TotalsByColorEntry
	3, // 5: DiceSets.DiceSet:type_name -> DiceSet
	0, // 6: Roller.Roll:input_type -> RollRequest
	1, // 7: Roller.Roll:output_type -> RollResponse
	7, // [7:8] is the sub-list for method output_type
	6, // [6:7] is the sub-list for method input_type
	6, // [6:6] is the sub-list for extension type_name
	6, // [6:6] is the sub-list for extension extendee
	0, // [0:6] is the sub-list for field type_name
}

func init() { file_dicelang_proto_init() }
func file_dicelang_proto_init() {
	if File_dicelang_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_dicelang_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RollRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dicelang_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RollResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dicelang_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Dice); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dicelang_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DiceSet); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dicelang_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DiceSets); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dicelang_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RollError); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_dicelang_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_dicelang_proto_goTypes,
		DependencyIndexes: file_dicelang_proto_depIdxs,
		MessageInfos:      file_dicelang_proto_msgTypes,
	}.Build()
	File_dicelang_proto = out.File
	file_dicelang_proto_rawDesc = nil
	file_dicelang_proto_goTypes = nil
	file_dicelang_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// RollerClient is the client API for Roller service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type RollerClient interface {
	// Rolls dice
	Roll(ctx context.Context, in *RollRequest, opts ...grpc.CallOption) (*RollResponse, error)
}

type rollerClient struct {
	cc grpc.ClientConnInterface
}

func NewRollerClient(cc grpc.ClientConnInterface) RollerClient {
	return &rollerClient{cc}
}

func (c *rollerClient) Roll(ctx context.Context, in *RollRequest, opts ...grpc.CallOption) (*RollResponse, error) {
	out := new(RollResponse)
	err := c.cc.Invoke(ctx, "/Roller/Roll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RollerServer is the server API for Roller service.
type RollerServer interface {
	// Rolls dice
	Roll(context.Context, *RollRequest) (*RollResponse, error)
}

// UnimplementedRollerServer can be embedded to have forward compatible implementations.
type UnimplementedRollerServer struct {
}

func (*UnimplementedRollerServer) Roll(context.Context, *RollRequest) (*RollResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Roll not implemented")
}

func RegisterRollerServer(s *grpc.Server, srv RollerServer) {
	s.RegisterService(&_Roller_serviceDesc, srv)
}

func _Roller_Roll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RollRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RollerServer).Roll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Roller/Roll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RollerServer).Roll(ctx, req.(*RollRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Roller_serviceDesc = grpc.ServiceDesc{
	ServiceName: "Roller",
	HandlerType: (*RollerServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Roll",
			Handler:    _Roller_Roll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "dicelang.proto",
}
