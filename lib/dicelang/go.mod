module gitlab.com/dicemagic/dicemagic/lib/dicelang

go 1.16

replace gitlab.com/dicemagic/dicemagic/lib/dicelang-errors v0.0.0 => ../dicelang-errors

require (
	github.com/aasmall/word2number v0.0.0-20180508050052-3e177d961031
	gitlab.com/dicemagic/dicemagic/lib/dicelang-errors v0.0.0
	gonum.org/v1/gonum v0.9.1
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
)
