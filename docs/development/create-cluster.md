# How-To Create a new cluster

Written for GKE clusters.

## create the cluster

1) Enable workload identity on cluster and node pools

## Ensure kubectl is up to date

```bash
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
```

## Install FluxV2 locally

```bash
curl -s https://toolkit.fluxcd.io/install.sh | sudo bash
```

## Install FluxV2 to cluster

https://toolkit.fluxcd.io/guides/installation/#gitlab-and-gitlab-enterprise


## create slack secrets

enable KMS API

```bash
gcloud services enable cloudkms.googleapis.com
```

create KMS key

```bash
gcloud kms keyrings create "dice-magic" --location "us-central1"
gcloud kms keys create "slack" \
    --location "us-central1" \
    --keyring "dice-magic" \
    --purpose encryption
```

ensure there is a place to put the secrets for the generator

```bash
mkdir -p ./config/development/secrets
```

encrypt secrets from slack

```bash
DEV_CLIENT_SECRET=[FROM https://api.slack.com/apps]
DEV_SIGNING_SECRET=[FROM https://api.slack.com/apps]
PROD_CLIENT_SECRET=[FROM https://api.slack.com/apps]
PROD_SIGNING_SECRET=[FROM https://api.slack.com/apps]

TMPFILE1=$(mktemp);TMPFILE2=$(mktemp)
printf ${DEV_CLIENT_SECRET} > ${TMPFILE1};printf ${DEV_SIGNING_SECRET} > ${TMPFILE2}
gcloud kms encrypt \
    --location "us-central1" \
    --keyring "dice-magic" \
    --key "slack" \
    --plaintext-file ${TMPFILE1} \
    --ciphertext-file ../flux/apps/development/secrets/slack-client-secret
gcloud kms encrypt \
    --location "us-central1" \
    --keyring "dice-magic" \
    --key "slack" \
    --plaintext-file ${TMPFILE2} \
    --ciphertext-file ../flux/apps/development/secrets/slack-signing-secret
rm  ${TMPFILE1} ${TMPFILE2}
TMPFILE1=$(mktemp);TMPFILE2=$(mktemp)
printf ${PROD_CLIENT_SECRET} > ${TMPFILE1};printf ${PROD_SIGNING_SECRET} > ${TMPFILE2}
gcloud kms encrypt \
    --location "us-central1" \
    --keyring "dice-magic" \
    --key "slack" \
    --plaintext-file ${TMPFILE1} \
    --ciphertext-file ../flux/apps/production/secrets/slack-client-secret
gcloud kms encrypt \
    --location "us-central1" \
    --keyring "dice-magic" \
    --key "slack" \
    --plaintext-file ${TMPFILE2} \
    --ciphertext-file ../flux/apps/production/secrets/slack-signing-secret
rm  ${TMPFILE1} ${TMPFILE2}
```

## Create GSA and Bind KSA to GSA

```bash
PROJECT_ID=dice-magic-v1
gcloud config set project ${PROJECT_ID}
gcloud iam service-accounts create dicemagic-ksa
gcloud iam service-accounts create certbot-ksa 
gcloud iam service-accounts add-iam-policy-binding \
  --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:dice-magic-v1.svc.id.goog[production/dicemagic-serviceaccount]" \
  dicemagic-ksa@dice-magic-v1.iam.gserviceaccount.com
gcloud iam service-accounts add-iam-policy-binding \
  --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:dice-magic-v1.svc.id.goog[development/dicemagic-serviceaccount]" \
  dicemagic-ksa@dice-magic-v1.iam.gserviceaccount.com
gcloud iam service-accounts add-iam-policy-binding \
  --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:dice-magic-v1.svc.id.goog[production/certbot-ksa]" \
  certbot-ksa@dice-magic-v1.iam.gserviceaccount.com
gcloud iam service-accounts add-iam-policy-binding \
  --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:dice-magic-v1.svc.id.goog[development/certbot-ksa]" \
  certbot-ksa@dice-magic-v1.iam.gserviceaccount.com

```

## enable datastore

```bash
gcloud services enable datastore.googleapis.com
```

open `https://console.cloud.google.com/datastore/setup?project=dice-magic-v1` in a browser and select `datastore` then pick a sensible location.

grant datastore permissions to GSA

```bash
PROJECT_ID=dice-magic-v1
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --role roles/datastore.user \
  --member serviceAccount:dicemagic-ksa@${PROJECT_ID}.iam.gserviceaccount.com
```

## create static IP and update local config file

```bash
gcloud compute addresses create dice-magic-ip --region us-central1
IP=$(gcloud compute addresses describe dicemagic-ip --region us-central1 | awk '$1=="address:" {print $2}')
sed -i --regexp-extended "s/loadBalancerIP: \".*\"/loadBalancerIP: \"${IP}\"/g" \
    config/development/nginx-loadbalancer.yaml
```

## point DNS at static IP

## run certbot job

grant certbot permissions

```bash
PROJECT_ID=dice-magic-v1
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --role roles/dns.admin \
  --member serviceAccount:certbot-ksa@${PROJECT_ID}.iam.gserviceaccount.com
```

run the job to get certs once

```bash
kubectl create job --from=cronjob/letsencrypt-job certbot
```

## grant dicemagic-serviceaccount permission to decrypt

dicemagic-ksa@dicemagic-dev.iam.gserviceaccount.com
roles/cloudkms.cryptoKeyDecrypter

```bash
PROJECT_ID=dicemagic-dev
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --role roles/cloudkms.cryptoKeyDecrypter \
  --member serviceAccount:dicemagic-ksa@${PROJECT_ID}.iam.gserviceaccount.com
PROJECT_ID=dicemagic-dev
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --role roles/cloudkms.cryptoKeyEncrypter \
  --member serviceAccount:dicemagic-ksa@${PROJECT_ID}.iam.gserviceaccount.com
```

## base64 encrypted secrets and add to CI config

SLACK_CLIENT_SECRET

```bash
base64 config/development/secrets/slack/slack-client-secret -w 0 | xclip -sel clip
```

SLACK_SIGNING_SECRET

```bash
base64 config/development/secrets/slack/slack-signing-secret -w 0 | xclip -sel clip
```

SELF_CERT

```bash
base64 config/development/secrets/letsencrypt-certs/tls.crt -w 0 | xclip -sel clip
```

SELF_KEY

```bash
base64 config/development/secrets/letsencrypt-certs/tls.key -w 0 | xclip -sel clip
```

for example:

![screenshot of git-lab CI variables config](ci-screenshot.png "add a variable")
