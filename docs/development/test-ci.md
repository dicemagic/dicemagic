# How-To: Test GitLab CI Locally

## Install a local GitLab runner

```bash
sudo pacman -S gitlab-runner
```
