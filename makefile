BUILD_DIR ?= ./out
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOVET=$(GOCMD) vet
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get

PROTOCZIP=protoc-3.12.0-linux-x86_64.zip
PROTOCDLOC=https://github.com/protocolbuffers/protobuf/releases/download/v3.12.0/

LIBS=$(shell find "lib" -type f)
ALL=$(shell find -type f)
GENDEPS=$(shell grep -rl . -e '//go:generate' ; find -type f -name "*.proto" )
GIT_COMMIT=$(shell git rev-list -1 HEAD)

VERSION=$(shell git describe --all --always --long --dirty)

CLIENT_EXECUTABLE=out/bin/dicemagic
WINDOWS=$(CLIENT_EXECUTABLE)_windows_amd64.exe
LINUX=$(CLIENT_EXECUTABLE)_linux_amd64
DARWIN=$(CLIENT_EXECUTABLE)_darwin_amd64

$(shell mkdir -p $(BUILD_DIR))

.PHONY: ci
ci: gen core letsencrypt www

.PHONY: local
local: gen core www mocks secrets bootstrapsecrets

.PHONY: gen
gen: $(GENDEPS)
	@mkdir -p ${HOME}/.local
	export PATH=${HOME}/.local/bin:${PATH}
	echo $$PATH
	curl -sLO $(PROTOCDLOC)$(PROTOCZIP)
	unzip -qo $(PROTOCZIP) -d ${HOME}/.local
	rm $(PROTOCZIP)
	go get github.com/golang/protobuf/protoc-gen-go
	go get golang.org/x/tools/cmd/stringer
	PATH=${HOME}/.local/bin:${PATH} go generate ./...

windows: $(WINDOWS) ## Build for Windows
linux: $(LINUX) ## Build for Linux
darwin: $(DARWIN) ## Build for Darwin (macOS)

$(WINDOWS):$(shell find "cmd" -maxdepth 1 -type f | grep -E '.*\.(go|mod|sum)$$') $(LIBS)
	env GOOS=windows GOARCH=amd64 go build -o $(WINDOWS) -ldflags="-s -w -X main.version=$(VERSION)" gitlab.com/dicemagic/dicemagic/cmd
$(LINUX):$(shell find "cmd" -maxdepth 1 -type f | grep -E '.*\.(go|mod|sum)$$') $(LIBS)
	env GOOS=linux GOARCH=amd64 go build  -o $(LINUX) -gcflags "-N -l" -ldflags="-X main.version=$(VERSION)" gitlab.com/dicemagic/dicemagic/cmd
$(DARWIN):$(shell find "cmd" -maxdepth 1 -type f | grep -E '.*\.(go|mod|sum)$$') $(LIBS)
	env GOOS=darwin GOARCH=amd64 go build -o $(DARWIN) -ldflags="-s -w -X main.version=$(VERSION)" gitlab.com/dicemagic/dicemagic/cmd

out/bin/chat-clients: $(shell find "chat-clients" -maxdepth 1 -type f | grep -E '.*\.(go|mod|sum)$$') $(LIBS)
	go build -ldflags "-X main.gitCommitID=$(GIT_COMMIT)" -o $@ gitlab.com/dicemagic/dicemagic/chat-clients

out/bin/dice-server: $(shell find "dice-server" -maxdepth 1 -type f | grep -E '.*\.(go|mod|sum)$$') $(LIBS)
	go build -o $@ gitlab.com/dicemagic/dicemagic/dice-server

.PHONY: core
core: out/bin/chat-clients out/bin/dice-server windows linux darwin

out/bin/mocks/datastore: $(shell find "mocks/datastore" -maxdepth 1 -type f | grep -E '.*\.(go|mod|sum|sh)$$') config/minikube/secrets/google/k8s-dice-magic.json
	@mkdir -p out/include/mocks/datastore/
	go build -o $@ gitlab.com/dicemagic/dicemagic/mocks/datastore
	cp mocks/datastore/bootstrap-datastore.sh out/include/mocks/datastore/

out/bin/mocks/kms: $(shell find "mocks/kms" -maxdepth 1 -type f | grep -E '.*\.(go|mod|sum)$$')
	go build -o $@ gitlab.com/dicemagic/dicemagic/mocks/kms

out/bin/mocks/slack-client: $(shell find "mocks/slack-client" -maxdepth 1 -type f | grep -E '.*\.(go)$$')
	go build -o $@ gitlab.com/dicemagic/dicemagic/mocks/slack-client

out/bin/mocks/slack-server: $(shell find "mocks/slack-server" -maxdepth 1 -type f | grep -E '.*\.(go|mod|sum)$$')
	go build -o $@ gitlab.com/dicemagic/dicemagic/mocks/slack-server

.PHONY: mocks
mocks: out/bin/mocks/datastore out/bin/mocks/kms out/bin/mocks/slack-client out/bin/mocks/slack-server

.PHONY: letsencrypt
letsencrypt: $(shell find "letsencrypt" -maxdepth 1 -type f | grep -E '.*\.(json|sh)$$')
	@mkdir -p out/include/letsencrypt
	cp letsencrypt/deployment-patch-template.json letsencrypt/renewcerts.sh letsencrypt/secret-patch-template.json out/include/letsencrypt

.PHONY: www
www: $(shell find "www" -type f)
	@mkdir -p out/include/www/content/local out/include/www/content/dev out/include/www/content/prod
	cp www/nginx.conf out/include/www/nginx.conf
	cp www/mime.types out/include/www/mime.types
	go install github.com/gohugoio/hugo@latest
	hugo -s www/ -d ../out/include/www/content/local --config config-local.yaml
	hugo -s www/ -d ../out/include/www/content/dev --config config-dev.yaml
	hugo -s www/ -d ../out/include/www/content/prod --config config.yaml

config/minikube/secrets/letsencrypt-certs/tls.key config/minikube/secrets/letsencrypt-certs/tls.crt: config/minikube/openssl-letsencrypt.cnf
	@mkdir -p config/minikube/secrets/letsencrypt-certs
	@openssl ecparam -genkey -name prime256v1 -out ./config/minikube/secrets/letsencrypt-certs/tls.key      
	@openssl req -new -x509 -nodes -days 3750 -SHA384 -key ./config/minikube/secrets/letsencrypt-certs/tls.key -out ./config/minikube/secrets/letsencrypt-certs/tls.crt -config ./config/minikube/openssl-letsencrypt.cnf

config/minikube/secrets/mocks/tls.key config/minikube/secrets/mocks/tls.crt: config/minikube/openssl-mocks.cnf
	@mkdir -p config/minikube/secrets/mocks
	@openssl ecparam -genkey -name prime256v1 -out ./config/minikube/secrets/mocks/tls.key
	@openssl req -new -x509 -nodes -days 3750 -SHA384 -key ./config/minikube/secrets/mocks/tls.key -out ./config/minikube/secrets/mocks/tls.crt -config ./config/minikube/openssl-mocks.cnf

config/minikube/secrets/slack/slack-client-secret config/minikube/secrets/slack/slack-signing-secret: out/bin/mocks/kms
	@mkdir -p config/minikube/secrets/slack
	./out/bin/mocks/kms -cli -encrypt=slack-client-secret-value > ./config/minikube/secrets/slack/slack-client-secret
	./out/bin/mocks/kms -cli -encrypt=slack-signing-secret-value > ./config/minikube/secrets/slack/slack-signing-secret

config/minikube/secrets/google/k8s-dice-magic.json:
	@mkdir -p config/minikube/secrets/google
	curl https://gist.githubusercontent.com/aasmall/b9cb7a5d6c3c675d7c99b3c19082d4c8/raw/83bcaa1667546d74f68cb04c6442d8a4306a0a12/fake-google-creds.json -o ./config/minikube/secrets/google/k8s-dice-magic.json

.PHONY: secrets
secrets: config/minikube/secrets/slack/slack-client-secret config/minikube/secrets/slack/slack-signing-secret config/minikube/secrets/mocks/tls.key config/minikube/secrets/mocks/tls.crt config/minikube/secrets/letsencrypt-certs/tls.key config/minikube/secrets/letsencrypt-certs/tls.crt config/minikube/secrets/google/k8s-dice-magic.json

clean:
	rm -rf ./out/ ./config/minikube/secrets/
	rm lib/dicelang/dicelang.pb.go chat-clients/channeltype_string.go mocks/slack-server/clienttype_string.go
	