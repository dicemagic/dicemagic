
# Dice Magic

***Slack Bot for Rolling Complex Dice***

## Build Status

| Branch | Pipeline Status | Code Coverage |
| - | - | - |
| main        | [![Build Status](https://gitlab.com/aasmall/dicemagic/badges/main/pipeline.svg?style=flat-square)](https://gitlab.com/aasmall/dicemagic)        | ![coverage](https://gitlab.com/aasmall/dicemagic/badges/main/coverage.svg?style=flat-square)        |
| development | [![Build Status](https://gitlab.com/aasmall/dicemagic/badges/development/pipeline.svg?style=flat-square)](https://gitlab.com/aasmall/dicemagic) | ![coverage](https://gitlab.com/aasmall/dicemagic/badges/development/coverage.svg?style=flat-square) |

## Set up a development environment

***n.b.*** *written from the standpoint of a Manjaro user. The [Arch wiki](https://wiki.archlinux.org/) will help a lot if yo get stuck.*

### Prerequisites

- [Google Cloud SDK](https://cloud.google.com/sdk/downloads)
- [Go](https://golang.org/doc/install)
- [Kustomize](https://github.com/kubernetes-sigs/kustomize/blob/master/docs/INSTALL.md)
- [Skaffold](https://skaffold.dev/docs/install/)
- [Hugo](https://gohugo.io/getting-started/installing)
- [dnsmasq](https://wiki.archlinux.org/index.php/NetworkManager#dnsmasq) (or a ton of pain mucking about with /etc/hosts)

### Clone me

Clone this repo and change current directory to this repos root directory

### Install and Enable Docker

> ***n.b.***
>- *You may need to reboot after this step.*

 ```bash
 sudo pacman -S docker
 sudo usermod -aG docker $USER
 sudo systemctl start docker
 sudo systemctl enable docker
 ```

### Install Kubectl

```bash
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client
```

```bash
Client Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.5", GitCommit:"6b1d87acf3c8253c123756b9e61dac642678305f", GitTreeState:"clean", BuildDate:"2021-03-18T01:10:43Z", GoVersion:"go1.15.8", Compiler:"gc", Platform:"linux/amd64"}
```

### Install KVM and start libvirtd

```bash
sudo pacman -S virt-manager qemu vde2 ebtables dnsmasq bridge-utils openbsd-netcat
sudo groupadd libvirt
sudo usermod -aG libvirt $USER
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service
```

### Minikube

#### Install Minikube

```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
&& sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

#### Start Minikube

```bash
minikube start --vm-driver=kvm2 --cpus=2 --nodes 3 \
--addons registry ingress \
--insecure-registry "10.0.0.0/24" \
--insecure-registry "192.168.39.0/24" 
```

#### Verify Successful cluster creation

```bash
kubectl cluster-info
```

> ```bash
> Kubernetes master is running at https://XXX.XXX.XXX.XXX:8443
> KubeDNS is running at https://XXX.XXX.XXX.XXX:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
>
> To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
> ```

#### Install Helm Charts

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install redis-cluster bitnami/redis-cluster -f config/minikube/redis-cluster-values.yaml
```

### Dicemagic

#### Configure Go

```bash
go env -w GO111MODULE=on
echo 'export PATH=$PATH:'"$(go env GOPATH)/bin" >> ~/.bash_profile
```

##### Get Generators

first make sure that Go's bin directory is in your $PATH then:

```bash
go get github.com/golang/protobuf/protoc-gen-go
go get golang.org/x/tools/cmd/stringer
```

#### Build local Dicemagic

```bash
make build local
```

### Skaffold

> **n.b.**
>
>- Skaffold currently doesn't work on multi-node minikube clusters. See **[bug report](https://github.com/GoogleContainerTools/skaffold/issues/5702).**

```bash
skaffold dev --default-repo $(minikube ip):5000
```

### Configure dnsmasq

>**n.b.**
>
>- Steps written assuming [NetworkManager](https://wiki.archlinux.org/index.php/Network_configuration#Network_managers). If you aren't using NetworkManager, follow your distributions process for updating local dns resolver instead.
>
>- You'll need to do this every time you recreate the minikube cluster
>
>- This will not work in browser if you are using Firefox and "DNS over HTTPS"

#### Set dnsmasq as NetworkManager's dns resolver

Edit/create `/etc/NetworkManager/conf.d/dns.conf`

```toml
[main]
dns=dnsmasq
```

Get minikube's IP and configure dnsmasq to forward requests to it for local.dicemagic.io.

```bash
kubectl get ingress web-ingress -oyaml | \
  yq '.status.loadBalancer.ingress[0].ip' | \
  sed -e 's/^"//' -e 's/"$//'
```

> ```bash
> xxx.xxx.xxx.xxx
> ```

Edit/create `/etc/NetworkManager/dnsmasq.d/address.conf` Use minikube's IP from above

```
#/etc/NetworkManager/dnsmasq.d/address.conf
address=/local.dicemagic.io/xxx.xxx.xxx.xxx
```

Reload NetworkManager

```bash
sudo nmcli general reload
```

> Once you've created the file, and the services are up and running, you can update the file with this simple command. **Requires yq**.
>
> ```bash
> NEW_IP=$(kubectl get ingress web-ingress -oyaml | \
>   yq '.status.loadBalancer.ingress[0].ip' | \
>   sed -e 's/^"//' -e 's/"$//')
> sudo sed -i -E "s/(address=\/local\.dicemagic\.io\/)(.*)/\1${NEW_IP}/" \
>   /etc/NetworkManager/dnsmasq.d/address.conf
> sudo nmcli general reload
> ```
>
>&nbsp;

## Go write code

And submit a pull request.
